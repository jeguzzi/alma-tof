#ifndef _WHEELCHAIRTRACKER_H
#define _WHEELCHAIRTRACKER_H

/* This is the interface to the wheelchair tracking library.
   The TOF device must be powered and connected before any functions are called.
   The wheelchair tracker provides its own display functions automatically.
   See documentation pdf for more information.
*/

class TrackerInterface
{
public:
	/* Call to create and initialise the wheelchair tracker */
	static TrackerInterface* Create();
	/* Waits for then processes a frame of data from the TOF device and returns metadata for tracked objects */ 
	virtual void Process(char * outputBuffer, int outputBufferSize, int * bytesWritten) = 0;
	/* Call to calibrate the wheel chair tracker*/
	virtual void Calibrate() = 0;
	/* Must call destructor on object provided by Create function */
	virtual ~TrackerInterface() {};
};

#endif