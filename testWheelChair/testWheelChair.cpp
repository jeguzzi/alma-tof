// testWheelChair.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include "wheelchair.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	TrackerInterface * w;
	char buffer[5000];
	int maxBufferSize = 5000;
	int bytesWritten;
	w = TrackerInterface::Create();

	w->Calibrate();

	while(true)
	{
		w->Process(buffer,maxBufferSize,&bytesWritten);
		cout << buffer;
		if (kbhit()) break;
	}

	delete w;

	return 0;
}

