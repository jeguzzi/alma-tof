#include "stdafx.h"
#include "wheelchair.h"
#include "windows.h"
#include "winhttp.h"
#include <iostream>
#include <string>
#include <ctime>
#include <algorithm>
#include <conio.h>

#define SERVER L"192.168.1.114" //"isin12.dti.supsi.ch"
#define STATE L"worlds/ISC/map/states/L3S1"
#define PORT 5000
#define SEND_PERIOD 1000
#define NAME L"tracked_targets"
#define BUFFER_SIZE 5000

using namespace std;

BOOL send_data(HINTERNET hSession, wstring server, int port, wstring url, const char *data)
{
		BOOL bResults = FALSE;
        HINTERNET hConnect = NULL,
                  hRequest = NULL;
	if( hSession )
        {
                hConnect = WinHttpConnect(hSession, server.c_str(), port, 0);
        }
        else
        {
                cout << "No http session" << endl;
                return 0;
        }

        if(hConnect)
        {
                hRequest = WinHttpOpenRequest(hConnect, L"POST", url.c_str(), NULL, WINHTTP_NO_REFERER,
                                              WINHTTP_DEFAULT_ACCEPT_TYPES, 0);
        }
        else
        {
                wcout << "No connection to " << server << endl;
                return 1;
        }
        if(!hRequest)
        {
                wcout << "No request to " << url << endl;
                return 1;
        }

        BOOL b = WinHttpSendRequest(hRequest, L"content-type:application/json", -1, (LPVOID) data,
                                  strlen(data), strlen(data), NULL);
		
        if (!b)
            printf("Error %d has occurred.\n", GetLastError());
		else
			printf("Sent\n");
		if (hRequest) WinHttpCloseHandle(hRequest);
        if (hConnect) WinHttpCloseHandle(hConnect);
		return b;
        
}

void usage(char *p)
{
        printf("Usage: %s  [-s <send period> -n <attribute name> -d <server_address> -p <server_port> -u <state url> -c <calibrate>]\n", p);
        exit(-1);
}

int main(int argc, char *argv[])
{
        std::wstring name = NAME;
        std::wstring state_uri = STATE;
        std::wstring server = SERVER;
        int  port = PORT;
        int send_period = SEND_PERIOD;
        char *p = argv[0];
		BOOL calibrate = FALSE;

        for (int i = 1; i < argc; i+=2) {

                if(strcmp(argv[i],"-h")==0)
                {
                        usage(p);
                }
                else if(strcmp(argv[i], "-n")==0)
                {
                        name = std::wstring(argv[i+1], argv[i+1] + strlen(argv[i+1]));
                }
                else if(strcmp(argv[i], "-u")==0)
                {
                        state_uri = std::wstring(argv[i+1],argv[i+1] + strlen(argv[i+1]) );
                }
                else if(strcmp(argv[i], "-d")==0)
                {
                        server = std::wstring(argv[i+1],argv[i+1] + strlen(argv[i+1]));
                }
                else if(strcmp(argv[i], "-p")==0)
                {
                        if(sscanf(argv[i+1],"%d",&port)!=1)
                        {
                                usage(p);
                        }
                }
				else if(strcmp(argv[i], "-c")==0)
                {
                        if(sscanf(argv[i+1],"%d",&calibrate)!=1)
                        {
                                usage(p);
                        }
                }
                else if(strcmp(argv[i], "-s")==0)
                {
                        if(sscanf(argv[i+1],"%d",&send_period)!=1)
                        {
                                usage(p);
                        }
                }
                else
                {
                        usage(p);
                }
        }

		wstring url = state_uri + L"/" + name;

        char output_buffer[BUFFER_SIZE];
        char prev_buffer[BUFFER_SIZE];
        char json_buffer[BUFFER_SIZE+100];
        int bytes_written;

        HINTERNET hSession = NULL;

        // Use WinHttpOpen to obtain a session handle.
        hSession = WinHttpOpen(L"ALMA TOF Camera/1.0",
                               WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                               WINHTTP_NO_PROXY_NAME,
                               WINHTTP_NO_PROXY_BYPASS, 0);


        std::wcout << "The camera will send updates to " << server << ":" << port << L"/" << url <<" every " << send_period << " ms." << endl;

        std::clock_t last_sent = std::clock();

        TrackerInterface * w;
        w = TrackerInterface::Create();

		if(calibrate)
		{
			cout << "Calibrating" << endl;
			w-> Calibrate();
			cout << "Calibration done" << endl;
		}

        while(true)
        {
                w->Process(output_buffer, BUFFER_SIZE ,&bytes_written);
                double ms = 1000.0 * (std::clock()- last_sent) / CLOCKS_PER_SEC;
                if (ms > send_period) {
					
                        if(strcmp(prev_buffer, output_buffer)!=0)
                        {
                                strcpy(prev_buffer,output_buffer);
                                output_buffer[bytes_written-2]='\0';
								string str = string(output_buffer);
								str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
                                sprintf(json_buffer, "{\"value\": {\"xml\":\"%s\"}}", str.data());
								printf("Sending: %s\n",json_buffer);
                                send_data(hSession, server, port, url, json_buffer);
                                last_sent = std::clock();
                        }
                }
				if (kbhit()) break;
        }

        // Close open handles.
		if (hSession) WinHttpCloseHandle(hSession);

        return 0;
}
